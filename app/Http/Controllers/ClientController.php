<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function add() {
        return view('add');
    }

    public function save(Request $request) {
        $client = new Client;
        $client->name = $request->get('nome');
        $client->save();
        return view('add', ['msg' => 'Cliente ' . $request->get('nome') . ' gravado com sucesso!']);
    }
}
